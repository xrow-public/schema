#!/bin/bash
echo "Doing customization"

mkdir -p /opt/app-root/src/httpd-cfg || true

ls -lisa /opt/app-root/src/
whoami

cat <<\EOF > /opt/app-root/src/httpd-cfg/settings.conf 
DocumentRoot /opt/app-root/src/public
<Directory /opt/app-root/src>
    Options +Indexes
    Options +FollowSymLinks
    Options +MultiViews
    AllowOverride all
</Directory>
<Location /server-status>
  SetHandler server-status
  # Require host localhost
</Location>
EOF

if [[ "${HOST}" != "" ]]; then
    name=$(echo "${HOST}" | sed 's~:[0-9]*~~g')
    echo "Generate cert for ${name}"
    mkdir -p /opt/app-root/src/httpd-ssl/certs
    mkdir -p /opt/app-root/src/httpd-ssl/private
    openssl req -x509 -newkey rsa:4096 -keyout /opt/app-root/src/httpd-ssl/private/${name}.pem -out /opt/app-root/src/httpd-ssl/certs/${name}.pem -sha256 -days 3650 -nodes -subj "/CN=${name}" -addext "subjectAltName=DNS:${name},DNS:${name}:8443,DNS:${name}:443,DNS:localhost,IP:127.0.0.1"
fi
if [[ ${HOST} != "schema.example.com" ]]; then
    echo "Find files in /opt/app-root/src"
    for file in $(find /opt/app-root/src -regextype posix-extended -iregex '.*[.]json' | sort -g); do 
        sed -i "s/schema.example.com/${HOST}/" $file
        echo "Modified: $file"
    done
fi