{{/*
Workaround: https://github.com/helm/helm/issues/3920#issuecomment-493530106
*/}}
{{- define "call-nested" }}
{{- $dot := index . 0 }}
{{- $subchart := index . 1 }}
{{- $template := index . 2 }}
{{- include $template (dict "Chart" (dict "Name" $subchart) "Values" (index $dot.Values $subchart) "Release" $dot.Release "Capabilities" $dot.Capabilities) }}
{{- end }}

{{/*
Auxiliary function to get the right value for existingSecret.

Usage:
{{ include "common.include.template" (dict "context" $ "template" "_pod.tpl" ) }}
Params:
  - template - string - Required. Nname of template in common
*/}}
{{- define "common.include.template" -}}
{{ fail ($.Files.Get (print "templates/" .template ) ) }}

{{ tpl ($.Files.Get (print "templates/" .template ) ) .context }}
{{- end }}

{{- define "common.include.pod" -}}
{{ range $path, $_ :=  $.Files.Glob  "**/*.yaml" }}
      {{ $path }}
{{ end }}
{{ include (print .Template.BasePath "/_pod.yaml") . | sha256sum }}
{{- end }}

{{/*
Kubernetes standard checksums
{{ include "common.template.checksums" (dict "templates" ( list "configmap.yaml" ) "context" $) -}}
*/}}
{{- define "common.template.checksums" -}}
{{- if and (hasKey . "templates") (hasKey . "context") -}}
{{- $context := .context -}}
{{- range .templates | default list -}}
checksum/{{ . | trimPrefix "/" }}: {{ include (print $context.Template.BasePath "/" (. | trimPrefix "/")) $context  | sha256sum | trunc 63 }}
{{- end -}}
{{- end -}}
{{- end -}}
