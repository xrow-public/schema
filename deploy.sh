#!/bin/bash

source /etc/environment

helm template helm-schema ./chart -f chart/tests/test.yaml

helm upgrade --install helm-schema ./chart -n helm-schema --create-namespace -f chart/tests/test.yaml

helm upgrade --install helm-schema-test ./schema-test -n helm-schema --create-namespace -f chart/tests/test.yaml