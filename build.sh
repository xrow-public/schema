#!/bin/bash

SCHEMA_DIR="$( dirname -- "$0"; )/"

chown -R 1001:1001 ${SCHEMA_DIR}src/

if [[ -v RH_REGISTRY_ACCOUNT && -v RH_REGISTRY_PASSWORD ]]; then
  echo "Login into RH registry registry.redhat.io ..."
  echo "${RH_REGISTRY_PASSWORD}" | podman login -u ${RH_REGISTRY_ACCOUNT} --password-stdin registry.redhat.io
else
  echo "RH_REGISTRY_ACCOUNT adn RH_REGISTRY_PASSWORD is not set. registry.redhat.io will be not available."
fi

export SCHEMA_HOST="${SCHEMA_HOST:-schema.example.com}"
podman stop schema || true
podman rm schema || true
podman build -t schema ${SCHEMA_DIR}
podman run -d --name schema -h ${SCHEMA_HOST} -e "HOST=${SCHEMA_HOST}" -v ${SCHEMA_DIR}src/:/opt/app-root/src/:z -p 443:8443 -p 80:8080 schema
curl --head -X GET --ipv4 --retry 10 --retry-connrefused --retry-delay 5 -k --fail https://localhost:443 || ( echo "Server did not start"; exit 1; )
openssl s_client -showcerts -connect localhost:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > /etc/pki/ca-trust/source/anchors/server.crt
update-ca-trust
grep -qF -- "127.0.0.1 ${SCHEMA_HOST}" "/etc/hosts" || echo "127.0.0.1 ${SCHEMA_HOST}" >> "/etc/hosts"

echo "Ready, now run:"
echo "# helm lint schema-test/"
echo "# helm lint chart/"
